import React, { Component } from 'react'
import produtos from '../../products.json'

export default class ListItens extends Component {
  constructor(props) {
      super(props)
  }

  listItems = () => {
    return produtos.products.map((item, index) => {
        return (
            <div className="col-md-4" key={index}>
                <div className="card">
                    <img className="card-img-top" src={item.image} alt="Card image cap" />
                    <div className="card-body">
                        <p className="card-title">{item.name}</p>
                        <div>
                            <span className="badge badge-danger mr-2 old">{item.regular_price}</span>
                            <span className="badge badge-light">{item.actual_price}</span>
                        </div>
                        <a href="#" className="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        );
    })
  }
  render() {
    
    return (
      <div className="row">
        {this.listItems()}
      </div>
    )
  }
}
