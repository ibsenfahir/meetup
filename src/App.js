import React, { Component } from 'react';
import ListItems from './componets/ListItems/ListItems'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container">
        <ListItems />
      </div>
    );
  }
}

export default App;
